package com.itnove.basic;

import java.util.Scanner;

public class Exercici1 {
    public static void main(String[] args) {
        int torn = 0;
        boolean seguir = true;
        //while (true) {
        //while (torn < 5) {
        while(seguir)  {
            //System.out.println("Estic al torn "+torn);
            Scanner sc = new Scanner(System.in);// se declara e inicializa una instancia  de la clase Scanner.
            System.out.println("Entrar Graus Celsius:  ");
            double k;
            double c = sc.nextDouble();
            k = c + 273.15;
            System.out.println(c + " Celsius son " + k + " Kelvin");
            //torn++;
            seguir = continuem();
        }
    }

    private static boolean continuem() {
        Scanner sc = new Scanner(System.in);// se declara e inicializa una instancia  de la clase Scanner.
        System.out.println("Continuem (S/N):  ");
        String c = sc.next();
        if(c.equals("S")) return true;
        else return false;
    }
}
