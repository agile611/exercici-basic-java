package com.itnove.basic;

import java.util.Scanner;

public class Exercici2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);// se declara e inicializa una instancia  de la clase Scanner.
        System.out.println("Entrar numero:  ");
        int c = sc.nextInt();
        for(int i=0; i <= 10;i++){
            System.out.println(c + " x  " + i + " = " + (c*i));
        }
    }
}
