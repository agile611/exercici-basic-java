package com.itnove.basic;

import java.util.Scanner;

public class Exercici4 {

    public static boolean esPerfecto(int n){
        int i, suma = 0;
        for (i = 1; i < n; i++) {  // i son los divisores. Se divide desde 1 hasta n-1
            if (n % i == 0) {
                suma = suma + i;     // si es divisor se suma
            }
        }
        if (suma == n) {  // si el numero es igual a la suma de sus divisores es perfecto
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        for(int i=0; i <= 1000; i++){
            if(esPerfecto(i)){
                System.out.println("Soy perfecto y soy el "+i);
            }
        }
    }
}
